from os import listdir
from time import sleep
from telepot import Bot
from telepot.loop import MessageLoop

# Save Bot using Summarizeit_Bot token
bot = []


def handle(msg):
    global bot
    try:
        chatid = msg['chat']['id']
        bot.sendMessage(chatid, "Hi!\nYour message to me was: " + msg['text'])
    except Exception:
        pass


if __name__ == "__main__":
    # global bot
    print "+===================== LISTING HOME"
    print os.listdir('/home')
    token = open('/home/.token', 'rb').read().rstrip()
    print "=========== TOKEN==============="
    print token
    bot = Bot(token)
    MessageLoop(bot, handle).run_as_thread()
    while(True):
        sleep(1)
